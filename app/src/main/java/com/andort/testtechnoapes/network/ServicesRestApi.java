package com.andort.testtechnoapes.network;

import com.andort.testtechnoapes.model.MoviesResponse;
import com.andort.testtechnoapes.util.Constants;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ServicesRestApi {

    @GET(Constants.Connection.MOVIES)
    Observable<MoviesResponse> getMovies(@Query("api_key") String api_key,
                                         @Query("format") String format);
}

package com.andort.testtechnoapes.repository;

import com.andort.testtechnoapes.base.BaseRepository;
import com.andort.testtechnoapes.network.ServicesRestApi;


import javax.inject.Inject;

import io.reactivex.Observable;

public class HomeRepository extends BaseRepository {

    @Inject
    public ServicesRestApi servicesRestApi;

    public Observable getMoviesService(String api_key, String format) {
        return servicesRestApi.getMovies(api_key, format);
    }

}

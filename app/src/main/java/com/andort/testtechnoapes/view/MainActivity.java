package com.andort.testtechnoapes.view;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.andort.testtechnoapes.R;
import com.andort.testtechnoapes.databinding.ActivityMainBinding;
import com.andort.testtechnoapes.viewmodel.HomeViewModel;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        HomeViewModel homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setHomeViewModel(homeViewModel);

        binding.recycleList.setLayoutManager(new LinearLayoutManager(this));

        homeViewModel.getMoviesService();

    }


}

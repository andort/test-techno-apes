package com.andort.testtechnoapes.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andort.testtechnoapes.R;
import com.andort.testtechnoapes.databinding.ItemBinding;
import com.andort.testtechnoapes.model.MoviesResponse;
import com.andort.testtechnoapes.model.Results;
import com.andort.testtechnoapes.viewmodel.ItemsMoviesViewModel;

import java.util.List;

import retrofit2.adapter.rxjava2.Result;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private Context context;
    private List<Results> items;

    public MainAdapter() {
    }


    public void updateList(List<Results> data){
        this.items = data;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        ItemBinding itemBinding;
        itemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item, viewGroup, false);
        return new ViewHolder(itemBinding);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(items.get(i));
    }

    @Override
    public int getItemCount() {
        if(items != null){
            return items.size();
        } else {
            return 0;
        }

    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private ItemBinding itemBinding;

        public ViewHolder(ItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.itemBinding = itemBinding;
        }

        ItemsMoviesViewModel itemsMoviesViewModel = new ItemsMoviesViewModel();

        public void bind(Results data){
            itemsMoviesViewModel.bind(data);
            itemBinding.setItemsMoviesViewModel(itemsMoviesViewModel);
        }
    }
}





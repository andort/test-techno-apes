package com.andort.testtechnoapes.model;

import android.os.Parcel;
import android.os.Parcelable;

class Producers implements Parcelable {

    private String api_detail_url;
    private int id;
    private String name;
    private String site_detail_url;

    public Producers(String api_detail_url, int id, String name, String site_detail_url) {
        this.api_detail_url = api_detail_url;
        this.id = id;
        this.name = name;
        this.site_detail_url = site_detail_url;
    }

    protected Producers(Parcel in) {
        api_detail_url = in.readString();
        id = in.readInt();
        name = in.readString();
        site_detail_url = in.readString();
    }

    public static final Creator<Producers> CREATOR = new Creator<Producers>() {
        @Override
        public Producers createFromParcel(Parcel in) {
            return new Producers(in);
        }

        @Override
        public Producers[] newArray(int size) {
            return new Producers[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(api_detail_url);
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(site_detail_url);
    }
}

package com.andort.testtechnoapes.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

public class Results implements Parcelable {

    private String api_detail_url;
    private String box_office_revenue;
    private String budget;
    private String date_added;
    private String date_last_updated;
    private String deck;
    private String description;
    //private Distributor distributor;
    //private HasStaffReview has_staff_review;
    private int id;
    private AllImage image;
    private String name;
    private List<Producers> producers;
    private String rating;
    private String release_date;
    private String runtime;
    private String site_detail_url;
    private List<Producers> studios;
    private String total_revenue;
    private List<Producers> writers;

    public Results(String api_detail_url, String box_office_revenue, String budget, String date_added, String date_last_updated, String deck, String description, int id, AllImage image, String name, List<Producers> producers, String rating, String release_date, String runtime, String site_detail_url, List<Producers> studios, String total_revenue, List<Producers> writers) {
        this.api_detail_url = api_detail_url;
        this.box_office_revenue = box_office_revenue;
        this.budget = budget;
        this.date_added = date_added;
        this.date_last_updated = date_last_updated;
        this.deck = deck;
        this.description = description;
        this.id = id;
        this.image = image;
        this.name = name;
        this.producers = producers;
        this.rating = rating;
        this.release_date = release_date;
        this.runtime = runtime;
        this.site_detail_url = site_detail_url;
        this.studios = studios;
        this.total_revenue = total_revenue;
        this.writers = writers;
    }

    protected Results(Parcel in) {
        api_detail_url = in.readString();
        box_office_revenue = in.readString();
        budget = in.readString();
        date_added = in.readString();
        date_last_updated = in.readString();
        deck = in.readString();
        description = in.readString();
        id = in.readInt();
        image = in.readParcelable(AllImage.class.getClassLoader());
        name = in.readString();
        producers = in.createTypedArrayList(Producers.CREATOR);
        rating = in.readString();
        release_date = in.readString();
        runtime = in.readString();
        site_detail_url = in.readString();
        studios = in.createTypedArrayList(Producers.CREATOR);
        total_revenue = in.readString();
        writers = in.createTypedArrayList(Producers.CREATOR);
    }

    public static final Creator<Results> CREATOR = new Creator<Results>() {
        @Override
        public Results createFromParcel(Parcel in) {
            return new Results(in);
        }

        @Override
        public Results[] newArray(int size) {
            return new Results[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(api_detail_url);
        parcel.writeString(box_office_revenue);
        parcel.writeString(budget);
        parcel.writeString(date_added);
        parcel.writeString(date_last_updated);
        parcel.writeString(deck);
        parcel.writeString(description);
        parcel.writeInt(id);
        parcel.writeParcelable(image, i);
        parcel.writeString(name);
        parcel.writeTypedList(producers);
        parcel.writeString(rating);
        parcel.writeString(release_date);
        parcel.writeString(runtime);
        parcel.writeString(site_detail_url);
        parcel.writeTypedList(studios);
        parcel.writeString(total_revenue);
        parcel.writeTypedList(writers);
    }

    public String getApi_detail_url() {
        return api_detail_url;
    }

    public void setApi_detail_url(String api_detail_url) {
        this.api_detail_url = api_detail_url;
    }

    public String getBox_office_revenue() {
        return box_office_revenue;
    }

    public void setBox_office_revenue(String box_office_revenue) {
        this.box_office_revenue = box_office_revenue;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getDate_added() {
        return date_added;
    }

    public void setDate_added(String date_added) {
        this.date_added = date_added;
    }

    public String getDate_last_updated() {
        return date_last_updated;
    }

    public void setDate_last_updated(String date_last_updated) {
        this.date_last_updated = date_last_updated;
    }

    public String getDeck() {
        return deck;
    }

    public void setDeck(String deck) {
        this.deck = deck;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AllImage getImage() {
        return image;
    }

    public void setImage(AllImage image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Producers> getProducers() {
        return producers;
    }

    public void setProducers(List<Producers> producers) {
        this.producers = producers;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getSite_detail_url() {
        return site_detail_url;
    }

    public void setSite_detail_url(String site_detail_url) {
        this.site_detail_url = site_detail_url;
    }

    public List<Producers> getStudios() {
        return studios;
    }

    public void setStudios(List<Producers> studios) {
        this.studios = studios;
    }

    public String getTotal_revenue() {
        return total_revenue;
    }

    public void setTotal_revenue(String total_revenue) {
        this.total_revenue = total_revenue;
    }

    public List<Producers> getWriters() {
        return writers;
    }

    public void setWriters(List<Producers> writers) {
        this.writers = writers;
    }
}

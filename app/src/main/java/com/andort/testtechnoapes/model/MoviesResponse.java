package com.andort.testtechnoapes.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class MoviesResponse implements Parcelable {

    private String error;
    private int limit;
    private int offset;
    private int number_of_page_results;
    private int number_of_total_results;
    private int status_code;
    private List<Results> results;
    private String version;


    protected MoviesResponse(Parcel in) {
        error = in.readString();
        limit = in.readInt();
        offset = in.readInt();
        number_of_page_results = in.readInt();
        number_of_total_results = in.readInt();
        status_code = in.readInt();
        results = in.createTypedArrayList(Results.CREATOR);
        version = in.readString();
    }

    public static final Creator<MoviesResponse> CREATOR = new Creator<MoviesResponse>() {
        @Override
        public MoviesResponse createFromParcel(Parcel in) {
            return new MoviesResponse(in);
        }

        @Override
        public MoviesResponse[] newArray(int size) {
            return new MoviesResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(error);
        parcel.writeInt(limit);
        parcel.writeInt(offset);
        parcel.writeInt(number_of_page_results);
        parcel.writeInt(number_of_total_results);
        parcel.writeInt(status_code);
        parcel.writeTypedList(results);
        parcel.writeString(version);
    }

    public String getError() {
        return error;
    }

    public int getLimit() {
        return limit;
    }

    public int getOffset() {
        return offset;
    }

    public int getNumber_of_page_results() {
        return number_of_page_results;
    }

    public int getNumber_of_total_results() {
        return number_of_total_results;
    }

    public int getStatus_code() {
        return status_code;
    }

    public List<Results> getResults() {
        return results;
    }

    public String getVersion() {
        return version;
    }
}

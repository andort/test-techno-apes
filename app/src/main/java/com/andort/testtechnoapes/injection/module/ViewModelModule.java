package com.andort.testtechnoapes.injection.module;

import com.andort.testtechnoapes.repository.HomeRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ViewModelModule {
    @Provides
    @Singleton
    HomeRepository provideAuthRepository() {
        return new HomeRepository();
    }

}

package com.andort.testtechnoapes.injection.module;

import com.andort.testtechnoapes.network.ServicesRestApi;
import com.andort.testtechnoapes.util.Constants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RepositoryModule {

    @Provides
    @Singleton
    ServicesRestApi provideServicesRestApi(Retrofit retrofit) {
        return retrofit.create(ServicesRestApi.class);
    }

    @Provides
    @Singleton
    Retrofit provideRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(Constants.Connection.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(new OkHttpClient.Builder().build())
                .build();
    }

}

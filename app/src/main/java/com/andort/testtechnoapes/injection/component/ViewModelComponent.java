package com.andort.testtechnoapes.injection.component;

import com.andort.testtechnoapes.injection.module.ViewModelModule;
import com.andort.testtechnoapes.viewmodel.HomeViewModel;
import com.andort.testtechnoapes.viewmodel.ItemsMoviesViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ViewModelModule.class})
public interface ViewModelComponent {
    void inject(HomeViewModel homeViewModel);
    void inject(ItemsMoviesViewModel itemsMoviesViewModel);
    @Component.Builder
    interface Builder {
        ViewModelComponent build();
        Builder injectModule(ViewModelModule viewModelModule);
    }
}

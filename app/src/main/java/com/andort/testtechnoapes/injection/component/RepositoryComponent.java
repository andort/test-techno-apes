package com.andort.testtechnoapes.injection.component;

import com.andort.testtechnoapes.injection.module.RepositoryModule;
import com.andort.testtechnoapes.repository.HomeRepository;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {RepositoryModule.class})
public interface RepositoryComponent {
    void inject(HomeRepository homeRepository);

    @Component.Builder
    interface Builder {
        RepositoryComponent build();
        Builder injectModule(RepositoryModule repositoryModule);
    }


}


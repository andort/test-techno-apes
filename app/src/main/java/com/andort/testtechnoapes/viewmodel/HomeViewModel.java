package com.andort.testtechnoapes.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.util.Log;

import com.andort.testtechnoapes.base.BaseViewModel;
import com.andort.testtechnoapes.model.MoviesResponse;
import com.andort.testtechnoapes.repository.HomeRepository;
import com.andort.testtechnoapes.util.Constants;
import com.andort.testtechnoapes.view.MainAdapter;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class HomeViewModel extends BaseViewModel {


    public HomeViewModel() {
    }

    @Inject
    public HomeRepository homeRepository;

    private Disposable subscription;
    private MutableLiveData<MoviesResponse> moviesResponse= new MutableLiveData<>();
    private MainAdapter mainAdapter = new MainAdapter();


    public MutableLiveData<MoviesResponse> getMoviesResponse() {
        return moviesResponse;
    }

    public void setMoviesResponse(MutableLiveData<MoviesResponse> moviesResponse) {
        this.moviesResponse = moviesResponse;
    }

    public void getMoviesService() {
        Observable<MoviesResponse> result = homeRepository.getMoviesService(Constants.Connection.API_KEY, "json");
        subscription = result.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(5)
                .subscribeWith(new DisposableObserver<MoviesResponse>() {

                    @Override
                    public void onNext(MoviesResponse moviesResponse) {
                        onLoginRetrieveSuccess(moviesResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        onLoginRetrieveFailure(e);
                    }

                    @Override
                    public void onComplete() {
                        Log.i("", "");
                    }
                });
    }

    private void onLoginRetrieveFailure(Throwable error) {
        if (error.getLocalizedMessage() != null) {
            System.out.println("Login error: " + error.getLocalizedMessage());
            //message.setValue(error.getLocalizedMessage());
        }

    }

    private void onLoginRetrieveSuccess(MoviesResponse data) {
        moviesResponse.setValue(data);
        mainAdapter.updateList(data.getResults());
    }

    public MainAdapter getMainAdapter() {
        return mainAdapter;
    }

    public void setMainAdapter(MainAdapter mainAdapter) {
        this.mainAdapter = mainAdapter;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (subscription != null) {
            subscription.dispose();
        }
    }



}

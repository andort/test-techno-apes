package com.andort.testtechnoapes.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import com.andort.testtechnoapes.base.BaseViewModel;
import com.andort.testtechnoapes.model.Results;

public class ItemsMoviesViewModel extends BaseViewModel {

    private Context context;
    private Results item;
    private MutableLiveData<String> imageMovie= new MutableLiveData<>();
    private MutableLiveData<String> title= new MutableLiveData<>();
    private MutableLiveData<String> releaseDate= new MutableLiveData<>();
    private MutableLiveData<String> runTime= new MutableLiveData<>();

    public MutableLiveData<String> getImageMovie() {
        return imageMovie;
    }

    public void setImageMovie(MutableLiveData<String> imageMovie) {
        this.imageMovie = imageMovie;
    }

    public MutableLiveData<String> getTitle() {
        return title;
    }

    public void setTitle(MutableLiveData<String> title) {
        this.title = title;
    }

    public MutableLiveData<String> getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(MutableLiveData<String> releaseDate) {
        this.releaseDate = releaseDate;
    }

    public MutableLiveData<String> getRunTime() {
        return runTime;
    }

    public void setRunTime(MutableLiveData<String> runTime) {
        this.runTime = runTime;
    }

    public void bind(Results items){
        this.item = items;
        imageMovie.setValue(items.getImage().getThumb_url());
        title.setValue(items.getName());
        releaseDate.setValue( items.getRelease_date());
        runTime.setValue(items.getRuntime() + " min");
    }


    public void viewDetail(){




    }




}

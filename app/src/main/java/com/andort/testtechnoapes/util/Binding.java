package com.andort.testtechnoapes.util;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;

public class Binding {

    @BindingAdapter("adapter")
    public static void setAdapter(RecyclerView recyclerView, RecyclerView.Adapter mAdapter){
        if(mAdapter != null){
            recyclerView.setAdapter(mAdapter);
        }
    }

    @BindingAdapter("image")
    public static void setImage(ImageView imageView, String url) {
        if (url != null) {
            Picasso.get().load(url).into(imageView);
        }
    }

}

package com.andort.testtechnoapes.base;

import com.andort.testtechnoapes.injection.component.DaggerRepositoryComponent;
import com.andort.testtechnoapes.injection.component.RepositoryComponent;
import com.andort.testtechnoapes.injection.module.RepositoryModule;
import com.andort.testtechnoapes.repository.HomeRepository;

public abstract class BaseRepository {
    public BaseRepository() {
        RepositoryComponent repositoryComponent = DaggerRepositoryComponent
                .builder()
                .injectModule(new RepositoryModule())
                .build();

        repositoryComponent.inject((HomeRepository) this);

    }

}

package com.andort.testtechnoapes.base;

import android.arch.lifecycle.ViewModel;

import com.andort.testtechnoapes.injection.component.DaggerViewModelComponent;
import com.andort.testtechnoapes.injection.component.ViewModelComponent;
import com.andort.testtechnoapes.injection.module.ViewModelModule;
import com.andort.testtechnoapes.viewmodel.HomeViewModel;
import com.andort.testtechnoapes.viewmodel.ItemsMoviesViewModel;

public abstract class BaseViewModel extends ViewModel {

    ViewModelComponent viewModelComponent = DaggerViewModelComponent
            .builder()
            .injectModule(new ViewModelModule())
            .build();


    public BaseViewModel() {
        if(this instanceof HomeViewModel){
            viewModelComponent.inject((HomeViewModel) this);
        }else if (this instanceof ItemsMoviesViewModel){
            viewModelComponent.inject((ItemsMoviesViewModel) this);
        }
    }
}
